# Kubernetes Fork Bomb

A chaos agent for stress testing a cluster or attempting to save it when moments
matter.

## Instructions

### Local

```bash
# install crds
make install

# create CR
kubectl apply -f config/samples

# run the controller (be ready to kill it)
make run
```
