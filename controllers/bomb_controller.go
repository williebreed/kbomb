/*
Copyright 2021 William Reed.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"os"

	"github.com/go-logr/logr"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	rbacv1beta1 "k8s.io/api/rbac/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	kbombv1alpha1 "gitlab.com/williebreed/kbomb/api/v1alpha1"
)

// BombReconciler reconciles a Bomb object
type BombReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
	bomb   *kbombv1alpha1.Bomb
}

// +kubebuilder:rbac:groups=kbomb.chaos.io,resources=bombs,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=kbomb.chaos.io,resources=bombs/status,verbs=get;update;patch
// +kubebuilder:rbac:groups="",resources=namespaces;configmaps,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=configmaps/status,verbs=get;update;patch
// +kubebuilder:rbac:groups="",resources=events,verbs=create
// +kubebuilder:rbac:groups=rbac.authorization.k8s.io,resources=clusterroles;clusterrolebindings;roles;rolebindings,verbs=get;list;watch;create;update;patch
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch

func (r *BombReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	_ = context.Background()
	_ = r.Log.WithValues("bomb", req.NamespacedName)

	r.bomb = &kbombv1alpha1.Bomb{}
	err := r.Client.Get(context.TODO(), req.NamespacedName, r.bomb)
	if err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}
	r.Log.Info("Reconciling", "name", r.bomb.Name, "namespace", r.bomb.Namespace)

	// Create child namespace
	ptrue := true
	ns := &v1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			GenerateName: req.Name,
			Labels: map[string]string{
				"chaos.io": "bomb",
			},
			OwnerReferences: []metav1.OwnerReference{{
				APIVersion:         "kbomb.chaos.io/v1alpha1",
				Kind:               "Bomb",
				Name:               r.bomb.Name,
				UID:                r.bomb.UID,
				Controller:         &ptrue,
				BlockOwnerDeletion: &ptrue,
			}},
		},
	}
	if err := r.Client.Create(context.TODO(), ns); err != nil {
		return ctrl.Result{}, err
	}
	r.Log.V(1).Info("Created", "type", "namespace", "name", ns.Name)

	// Plant bomb in ns ;)
	childBomb := &kbombv1alpha1.Bomb{
		ObjectMeta: metav1.ObjectMeta{
			Name:      req.Name,
			Namespace: ns.Name,
			OwnerReferences: []metav1.OwnerReference{{
				APIVersion:         "kbomb.chaos.io/v1alpha1",
				Kind:               "Bomb",
				Name:               r.bomb.Name,
				UID:                r.bomb.UID,
				Controller:         &ptrue,
				BlockOwnerDeletion: &ptrue,
			}},
		},
	}
	// Fun fact, this would be the right way to do this, but the controller
	// runtime rightfully prevents me from doing what I'm trying to do.  Which
	// is good because eventually I will damage the cluster by doing this.
	// if err := ctrl.SetControllerReference(bomb, childBomb, r.Scheme); err != nil {
	// 	return ctrl.Result{}, err
	// }
	if err := r.Client.Create(context.TODO(), childBomb); err != nil {
		return ctrl.Result{}, err
	}

	if InCluster() {
		return ctrl.Result{}, Replicate(r, ns)
	}

	return ctrl.Result{}, nil
}

func InCluster() bool {
	_, ret := os.LookupEnv("KUBERNETES_SERVICE_HOST")
	return ret
}

func Replicate(r *BombReconciler, target *v1.Namespace) error {
	controllerNamespace := os.Getenv("POD_NAMESPACE")

	r.Log.Info("Replicating", "from", controllerNamespace, "to", target.Name)
	resources := []struct {
		k types.NamespacedName
		o runtime.Object
	}{
		{
			k: types.NamespacedName{Name: "kbomb-leader-election-role", Namespace: controllerNamespace},
			o: &rbacv1beta1.Role{},
		},
		{
			k: types.NamespacedName{Name: "kbomb-leader-election-rolebinding", Namespace: controllerNamespace},
			o: &rbacv1beta1.RoleBinding{},
		},
		{
			k: types.NamespacedName{Name: "kbomb-manager-rolebinding"},
			o: &rbacv1beta1.ClusterRoleBinding{},
		},
		{
			k: types.NamespacedName{Name: "kbomb-proxy-rolebinding"},
			o: &rbacv1beta1.ClusterRoleBinding{},
		},
		{
			k: types.NamespacedName{Name: "kbomb-controller-manager", Namespace: controllerNamespace},
			o: &appsv1.Deployment{},
		},
	}

	for _, e := range resources {
		r.Log.Info("Looping over resources")
		r.Log.Info("getting", "resource", e.k)
		if err := r.Client.Get(context.TODO(), e.k, e.o); err != nil {
			return err
		}
		metaO := e.o.(metav1.Object)
		metaO.SetNamespace(target.Name)

		labels := metaO.GetLabels()
		if labels == nil {
			labels = make(map[string]string)
		}
		labels["chaos.io"] = "true"
		metaO.SetLabels(labels)
		metaO.SetCreationTimestamp(metav1.Time{})
		metaO.SetResourceVersion("")
		metaO.SetUID("")
		metaO.SetSelfLink("")

		switch obj := metaO.(type) {
		case *rbacv1beta1.RoleBinding:
			obj.Subjects[0].Namespace = target.Name
		case *rbacv1beta1.ClusterRoleBinding:
			obj.Subjects[0].Namespace = target.Name
			obj.GenerateName = obj.Name
			obj.Name = ""
			obj.Namespace = ""
		}

	}

	for _, e := range resources {
		metaO := e.o.(metav1.Object)
		r.Log.Info("Creating", "type", fmt.Sprintf("%T", e.o), "name", metaO.GetName(), "namespace", metaO.GetNamespace())
		if err := r.Client.Create(context.TODO(), e.o); err != nil {
			return err
		}
	}

	// time.Sleep(999999999999)

	return nil
}

func (r *BombReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&kbombv1alpha1.Bomb{}).
		Complete(r)
}
